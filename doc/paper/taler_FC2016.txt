
-------- Forwarded Message --------
Subject: FC 2016 reviews for paper 10
Date: Wed, 2 Dec 2015 11:19:05 +0100
From: FC 2016 <fc2016@easychair.org>
To: Christian Grothoff <grothoff@net.in.tum.de>

Dear Christian Grothoff,

Please find below the reviews for your paper
10 Taler: Taxable Anonymous Libre Electronic Reserves
that was not accepted to Financial Crypto 2016.

We hope that these comments are useful for your research and that you
can join us in Barbados in February.

Best regards,
Bart & Jens
Program co-chairs FC 2016


----------------------- REVIEW 1 ---------------------
PAPER: 10
TITLE: Taler: Taxable Anonymous Libre Electronic Reserves
AUTHORS: Florian Dold, Christian Grothoff, Benedikt Müller and Sree
Harsha Totakura


----------- REVIEW -----------
Positives: This paper is interesting, well-written, and accessible.

Drawbacks: The core technical contribution of the paper is a coin
refresh protocol that (i) is necessitated for making change and (ii)
goes to great lengths to avoid customers abusing it as a transaction
oracle.

The problem is that I think the paper fails on both (i) and (ii), but
mostly on (ii). A simple way to do (i) is requiring the user to go to
the mint first to make change (as per DigiCash). You might argue that
with Taler, the user can be offline even if the merchant is online: I
might buy this, but this argument isn’t made in the paper. Now this
arguably still requires linkability between the whole coin and the two
split coins however…

Regarding (ii): while Taler does prevent coin refreshes from being
abused, it does not seem to me to prevent the original withdrawal
procedure from such abuse. If Alice wants to pay Bob in a tax-free way,
she can take a blinded coin from Bob and withdraw it from the mint
herself. The mint thinks it is Alice’s coin but only Bob knows the key
in it, and so only Bob can spend it. Alice gives the coin to Bob to
complete the payment. This does not allow a chain of transactions, as
Bob has to do something with the coin, but generally digital cash
services let you return an unspent coin at any time and credit your
account, which Bob could do. But even if he can’t, at least one payment
can be laundered in this way.

Finally, I think the contribution here is somewhat narrow. Linkable
refreshing is done with a cut-and-choose and is not particularly
challenging once you know what you want to do (I suppose the
contribution is partly in developing the requirement, based on real
world requirements).

Other comments:

[1] I didn’t understand why ZeroCoin is particularly suited for
developing nations?

[1] Taxability: with reference to income tax, if Alice works at Acme Inc
and is paid her salary, in this case Acme Inc is the “customer” and
Alice is the “merchant”? Is that the idea? Otherwise it seems, the
taxability property should apply equally to customers and merchants.

[1] The change protocol sounds like it solving the same problem as
HINDE. While HINDE isn’t well documented, the authors should attempt to
contrast their approach with it. In HINDE, the customer creates coins to
withdraw (so only they can spend them) but the merchant pays for the
withdraw. These can be used as change. It is compatible with DigiCash.

[2.1] “easily taxable” -> this concept paints a picture of the tax
agency proactively looking at transactions. A better way of describing
it might be that it leaves an audit trail for tax agencies.

[2.1] There is no casual relationship that can be proven between
Bitcoin’s independence as a currency and its volatility. All you can
really say is that today, Bitcoin is more volatile than certain
currencies (and less so than others) but we have no idea why and if that
might change in the future.

[2.1] I don’t see AltCoins as a “problem.” You are correct that Bitcoin
is less a currency and more an open protocol for creating new
currencies. So what? And why do altcoins become a ponzi scheme? (Noting
that you do not say that they might become one, rather that they do).

[2.2] How does Taler avoid Chaum’s patent on his blind signature scheme?
It seems to be built on it. (Could you use Lucre instead?) (Or is it
that Chaum’s patent has expired?)

[2.2] I thought DigiCash used the Chaum-Fiat-Naor (or variant) scheme
for offline detection of double-spending? Even if it didn’t, you should
mention the possibility of using this kind of detection mechanism (and
variations from Ferguson, etc)

[2.2] Divisible e-cash is a subject with many publications beyond
Brands’ work. The authors should include a broader survey of this as it
seems pertinent. They should also consider anonymized change protocols,
as mentioned above, such as HINDE.

[3.1] To be clear, the anonymous channel only hides the customer’s
identity, not that of the merchant or mint? (Which is obviously what Tor
provides in its base form, without hidden services)

[3.1] Why does the customer need an anonymous channel when interacting
with the mint?

[3.2] The discussion of copying private keys is informative but I’m not
sure it is sufficient. If the signature scheme is one that admits
threshold signing (or even just distributed key generation), it might be
possible that entities own shares of a single private key in a way that
is indistinguishable from the situation where there is only one private
key. In this case, they do not have to worry about the other party
absolving with the funds (but they do have to worry about the other
party cooperating when one party wants to use the funds).

[3.3] I think you understate the benefits of the mint knowing the
identity of the customer: many countries have Know Your Customer (KYC)
laws for organizations like your mint—as many Bitcoin business are now
finding out about :) I would explicitly add KYC to your list of
requirements.

[3.4] In case of a loss mint private key, you say customers can exchange
their unspent coins. I think you either mean (i) their potentially
unspent coins (because the mint only has a list of <customer, amount>
and doesn’t know what was spent) or (ii) the bank keeps a record of the
blinded coins it has signed and the customer must spend their coin (to
prove it is unspent) and provide the blinding factor (to prove it was
issued and not made up with the leaked key). In either case, this needs
much more explanation (or a forward pointer if it is explained later).

[3.5] Is there any real difference between spending a fraction of a coin
a refreshing it, or going to the mint and exchanging a whole coin for
two new coins (one worth the value of the transaction and the other
worth the difference)? This is effectively how Digicash works. To link
the old (whole) coin to the new issuance, the customer could be required
to provide the blinding factors.

[4.1] IIRC Chaumian blind signatures are based on RSA. You are using
discrete logarithms (presumedly if you are using elliptic curves). Blind
sigs in the DL setting exist of course, but you should specify and cite
an appropriate one.

[4.6] If Alice pays $75 to Bob using a $100 coin, is there any technical
difference between (a) Bob limiting the coin to $75 and Alice refreshing
the coin and (b) Bob taking the $100 but issuing a $25 refund to Alice,
who then refreshes the refund?


----------------------- REVIEW 2 ---------------------
PAPER: 10
TITLE: Taler: Taxable Anonymous Libre Electronic Reserves
AUTHORS: Florian Dold, Christian Grothoff, Benedikt Müller and Sree
Harsha Totakura


----------- REVIEW -----------
This paper presents a number of important design ideas: it adapts
chaums' e-cash ideas to the modern settings, and augments it with modern
notions of anonymity for the spenders, traceability and accountability
for the merchant, the ability to levy tax, and features to prevent
fraud. A key assumption used, that makes it different from traditional
e-cash, is that on-line checks are expected, making traceability and
identity escrow unnecessary to prevent double spending.

The paper does present some good ideas: it is pragmatic about balancing
abuse prevention with privacy, and also recognizes that modern monetary
systems have to support taxation and known merchants. It also uses the
rule of law to enforce parts of contracts (such as delivery of goods)
rather that complicating the protocols with such things -- which other
designs attempt and fail to address in a satisfactory manner.

At the same time, the paper also has some serious issues, that prevent
me from wholeheartedly supporting its acceptance: first, it reads a
little like a white paper. The details of the crypto are a bit thin, and
it is not clear how to instantiate specifically the blind signatures and
other primitives proposed. Following from this, there is no evidence any
part of it has been implemented and evaluated for any system aspect --
performance, latency. This is a missed opportunity, as such an
implementation -- and its evaluation -- would provide a good reference
point to compare with the more expensive crypto-currency designs;
finally, the paper makes reference to blind signatures from Chaum, but
of course a number of constructions -- allowing for efficient proofs --
have been proposed since. It is not clear the authors appreciate their
importance or even existence.

However, providing proofs of the statement to be signed is important,
and a potential attack on the presented scheme may illustrate this. The
scheme suggests that a any transfers of value should be taxed. However,
the issuing protocol in 4.1 can be abused to transfer a coin, without
paying tax, and in an unlikable manner. The party withdrawing the coin
may chose to use a public key belonging to someone else in step 4 --
thus asking for a coin controlled by another entity to be signed by the
issuer. As a result, the coin can be directly used by the other party,
without any visible transfer (or use of the spending protocol). This
could be avoided by using a modern credential issuing protocol that
ensures the party withdrawing a coin, knows the secret associated with
the coin -- something that traditional chaum blind signatures can only
achieve with a cut-and-chose technique, which is very expensive.

So my advice would be to chose a modern credential scheme to instantiate
the protocol, such as the anonymous credential light (Baldimtsi et al)
protocols, actually implement the protocol, and then provide a thorough
security and performance evaluations.


----------------------- REVIEW 3 ---------------------
PAPER: 10
TITLE: Taler: Taxable Anonymous Libre Electronic Reserves
AUTHORS: Florian Dold, Christian Grothoff, Benedikt Müller and Sree
Harsha Totakura


----------- REVIEW -----------
It seems like the only novelty here has to do with the mechanism to
unlinkably refresh partially-spent coins. I can imagine that being
useful! But I'm not sure it would be useful. Its value should be
compared to on-line-verified DigiCash Ecash, to which it is most
similar, to Bitcoin (it is clearly better for payer-privacy than
Bitcoin) and to Zerocash. I think it is probably better than Zerocash in
some performance measures, and in avoiding the need for secure parameter
setup (which raises the possibility of a backdoor in Zerocash).

There are a lot of comparisons to Chaumian off-line
double-spending-detection, but those aren't as relevant as a comparison
to Ecash would be. The only difference in functionality between TALER
and Ecash as far as I can tell is the ability to spend a part of a coin.
It isn't clear to me how important that is.

But, this paper is rather weighed down by a lot of other stuff which is
not novel and/or of questionable value. DigiCash Ecash as deployed (not
as described in the original paper) already did on-line verification.

I object to the headlining motivation of "taxable". The scheme is
neither necessary nor sufficient for taxation, and should instead be
called something like "payer-anonymous, payee-auditable". As far as I
understand, there's nothing in TALER that makes it more amenable to
tracing/auditing (or as they call it "taxability") than Ecash. Both
DigiCash Ecash and TALER seem to be less traceable/auditable than Bitcoin.

A few positive comments:

Positive: explicitly mentions privacy risks: network (addressed with
Tor), mint-selection, merchant-customer metadata

Positive: explicitness about when durable writes ("commits") are needed,
and about resumption

Positive: explicitness about expiration/garbage-collection

Positive: explicitness about multiple mints



