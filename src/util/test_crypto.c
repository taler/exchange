/*
  This file is part of TALER
  (C) 2015 GNUnet e.V.

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * @file util/test_crypto.c
 * @brief Tests for Taler-specific crypto logic
 * @author Christian Grothoff <christian@grothoff.org>
 */
#include "platform.h"
#include "taler_util.h"
#include "taler_crypto_lib.h"


/**
 * Test high-level link encryption/decryption API.
 *
 * @return 0 on success
 */
static int
test_high_level ()
{
  struct GNUNET_CRYPTO_EddsaPrivateKey *pk;
  struct TALER_CoinSpendPrivateKeyP coin_priv;
  struct TALER_CoinSpendPublicKeyP coin_pub;
  struct GNUNET_CRYPTO_EcdhePrivateKey *pk2;
  struct TALER_TransferPrivateKeyP trans_priv;
  struct TALER_TransferPublicKeyP trans_pub;
  struct TALER_TransferSecretP secret;
  struct TALER_TransferSecretP secret2;
  struct TALER_FreshCoinP fc1;
  struct TALER_FreshCoinP fc2;

  pk = GNUNET_CRYPTO_eddsa_key_create ();
  coin_priv.eddsa_priv = *pk;
  GNUNET_free (pk);
  GNUNET_CRYPTO_eddsa_key_get_public (&coin_priv.eddsa_priv,
				      &coin_pub.eddsa_pub);
  pk2 = GNUNET_CRYPTO_ecdhe_key_create ();
  trans_priv.ecdhe_priv = *pk2;
  GNUNET_free (pk2);
  GNUNET_CRYPTO_ecdhe_key_get_public (&trans_priv.ecdhe_priv,
				      &trans_pub.ecdhe_pub);
  TALER_link_derive_transfer_secret (&coin_priv,
                                     &trans_priv,
                                     &secret);
  TALER_link_reveal_transfer_secret (&trans_priv,
                                     &coin_pub,
                                     &secret2);
  GNUNET_assert (0 ==
		 memcmp (&secret,
			 &secret2,
			 sizeof (secret)));
  TALER_link_recover_transfer_secret (&trans_pub,
                                      &coin_priv,
                                      &secret2);
  GNUNET_assert (0 ==
		 memcmp (&secret,
			 &secret2,
			 sizeof (secret)));
  TALER_setup_fresh_coin (&secret,
                          0,
                          &fc1);
  TALER_setup_fresh_coin (&secret,
                          1,
                          &fc2);
  GNUNET_assert (0 !=
                 memcmp (&fc1,
                         &fc2,
                         sizeof (struct TALER_FreshCoinP)));
  return 0;
}


int
main(int argc,
     const char *const argv[])
{
  if (0 != test_high_level ())
    return 1;
  return 0;
}

/* end of test_crypto.c */
