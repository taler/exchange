# This file is in the public domain.
#
[PATHS]
# Persistant data storage for the testcase
TALER_TEST_HOME = test_exchange_api_home/

[taler]
# Currency supported by the exchange (can only be one)
CURRENCY = EUR

[exchange]

# Wire format supported by the exchange
# We use 'test' for testing of the actual
# coin operations, and 'sepa' to test SEPA-specific routines.
WIREFORMAT = test sepa

# HTTP port the exchange listens to
PORT = 8081

# Master public key used to sign the exchange's various keys
MASTER_PUBLIC_KEY = 98NJW3CQHZQGQXTY3K85K531XKPAPAVV4Q5V8PYYRR00NJGZWNVG

# How to access our database
DB = postgres

# Is this is a testcase, use transient DB actions?
TESTRUN = YES

[exchangedb-postgres]
DB_CONN_STR = "postgres:///talercheck"

[exchange-wire-incoming-sepa]
# This is the response we give out for the /wire request.  It provides
# wallets with the bank information for transfers to the exchange.
SEPA_RESPONSE_FILE = ${TALER_CONFIG_HOME}/sepa.json

[exchange-wire-incoming-test]
# This is the response we give out for the /wire request.  It provides
# wallets with the bank information for transfers to the exchange.
TEST_RESPONSE_FILE = ${TALER_CONFIG_HOME}/test.json

[exchange-wire-outgoing-test]
# What is the main website of the bank?
BANK_URI = "http://localhost:8082/"
# From which account at the 'bank' should outgoing wire transfers be made?
BANK_ACCOUNT_NUMBER = 2

[coin_eur_ct_1]
value = EUR:0.01
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.00
fee_deposit = EUR:0.00
fee_refresh = EUR:0.01
fee_refund = EUR:0.01
rsa_keysize = 1024

[coin_eur_ct_10]
value = EUR:0.10
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024

[coin_eur_1]
value = EUR:1
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024

[coin_eur_5]
value = EUR:5
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024

[coin_eur_10]
value = EUR:10
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
